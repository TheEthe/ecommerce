package com.comtrade.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class Stavke {
	
	@EmbeddedId
	private StavkeId id;
	@ManyToOne
	@MapsId("porudzbinaId")
	private Porudzbina porudzbina;
	
	@ManyToOne
	@MapsId("artikalId")
	private Artikal artikal;
		
	private int kolicina;

	public Stavke() {
		
	}


	public Stavke(Porudzbina porudzbina, Artikal artikal) {
		super();	
		this.porudzbina = porudzbina;
		this.artikal = artikal;	
		id = new StavkeId(porudzbina.getId_porudzbine(), artikal.getId_artikla());
	}


	public StavkeId getId() {
		return id;
	}


	public void setId(StavkeId id) {
		this.id = id;
	}


	public Porudzbina getPorudzbina() {
		return porudzbina;
	}


	public void setPorudzbina(Porudzbina porudzbina) {
		this.porudzbina = porudzbina;
	}


	public Artikal getArtikal() {
		return artikal;
	}


	public void setArtikal(Artikal artikal) {
		this.artikal = artikal;
	}


	public int getKolicina() {
		return kolicina;
	}


	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	
}
