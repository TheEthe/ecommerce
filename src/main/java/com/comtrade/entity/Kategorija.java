package com.comtrade.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Kategorija {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_kategorije;
	
	private String naziv;
	
	@OneToMany(mappedBy = "kategorija", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Artikal> setArtikal = new HashSet<Artikal>();
	
	public void addArtikal(Artikal artikal) {
		setArtikal.add(artikal);
		artikal.setKategorija(this);
	}
	
	public void removeArtikal(Artikal artikal) {
		setArtikal.remove(artikal);
		artikal.setKategorija(null);
	}
	

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Long getId_kategorije() {
		return id_kategorije;
	}

	public void setId_kategorije(Long id_kategorije) {
		this.id_kategorije = id_kategorije;
	}

	public Set<Artikal> getSetArtikal() {
		return setArtikal;
	}

	public void setSetArtikal(Set<Artikal> setArtikal) {
		this.setArtikal = setArtikal;
	}
	

}
