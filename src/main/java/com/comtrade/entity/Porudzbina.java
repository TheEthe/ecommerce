package com.comtrade.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Porudzbina {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_porudzbine;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usera")
	private User user;
	
	@OneToMany(mappedBy = "artikal", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Stavke> setStavkiP = new HashSet<Stavke>();
	
//	public void addArtikal(Artikal artikal) {
//		RacunStavke racunStavke = new RacunStavke(this, artikal);
//		setRacunaStavkeR.add(racunStavke);
//		artikal.getSetRacunaStavkeA().add(racunStavke);
//	}
//	
	
	public void addArtikal(Artikal artikal) {
		Stavke stavke = new Stavke(this, artikal);
		setStavkiP.add(stavke);
		artikal.getSetStavkiA().add(stavke);
		
	}
	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate datum;


	public Porudzbina() {
		super();
		datum = LocalDate.now();
	}


	public Long getId_porudzbine() {
		return id_porudzbine;
	}


	public void setId_porudzbine(Long id_porudzbine) {
		this.id_porudzbine = id_porudzbine;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public LocalDate getDatum() {
		return datum;
	}


	public void setDatum(LocalDate datum) {
		this.datum = datum;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
