package com.comtrade.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class StavkeId implements Serializable{

	private Long porudzbinaId;
	private Long artikalId;
	
	public StavkeId(Long porudzbinaId, Long artikalId) {
		super();
		this.porudzbinaId = porudzbinaId;
		this.artikalId = artikalId;
	}
	
	public StavkeId() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikalId == null) ? 0 : artikalId.hashCode());
		result = prime * result + ((porudzbinaId == null) ? 0 : porudzbinaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StavkeId other = (StavkeId) obj;
		if (artikalId == null) {
			if (other.artikalId != null)
				return false;
		} else if (!artikalId.equals(other.artikalId))
			return false;
		if (porudzbinaId == null) {
			if (other.porudzbinaId != null)
				return false;
		} else if (!porudzbinaId.equals(other.porudzbinaId))
			return false;
		return true;
	}
	
	
	
	
}
